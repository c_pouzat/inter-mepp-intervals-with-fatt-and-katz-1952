# Some exercices in statistical analysis from Fig. 12 of Fatt & Katz (1952)

We explore here the data used in Fig. 12 of [Fatt and Katz(1952)](https://physoc.onlinelibrary.wiley.com/doi/abs/10.1113/jphysiol.1952.sp004735). The data were originaly published by Cox and Lewis (1966) _The statistical analysis of series of events_, where they are wrongly described as _inter spike interval_ data; they are also used--with the same wrong description--in the two excellent books of Larry Wasserman, _All of Statistics_ and _All of Nonparametric Statistics_. 

## Content

- `data` contains a copy of file `nerve.dat` from L. Wasserman site with the [data](https://www.stat.cmu.edu/~larry/all-of-nonpar/data.html) for his _All of Nonparametric Statistics_ book.
- `src` contains the `Rmd` source file to reproduce and inspect the analysis.
- `public` contains the `HTML` file that can be visualized directly from [inter-mepp-intervals-with-fatt-and-katz-1952](https://c_pouzat.gitlab.io/inter-mepp-intervals-with-fatt-and-katz-1952/).

## Regenerating the analysis

To regenerate the analysis, go to `src`, start `R` and type:

```
library("rmarkdown")
rmarkdown::render("inter-mepp-intervals.Rmd",output_dir = "../public",output_file="index.html")
```
